﻿namespace FileSearchWinForms
{
    partial class Form
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.treeViewResult = new System.Windows.Forms.TreeView();
            this.folderSelectDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.timerSearch = new System.Windows.Forms.Timer(this.components);
            this.timerTreeUpdate = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.labelTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.LabelFileProcessing = new System.Windows.Forms.ToolStripStatusLabel();
            this.buttonStartSearch = new MetroFramework.Controls.MetroButton();
            this.buttonStopSearch = new MetroFramework.Controls.MetroButton();
            this.textBoxFolderName = new MetroFramework.Controls.MetroTextBox();
            this.textBoxFileTemplate = new MetroFramework.Controls.MetroTextBox();
            this.textBoxFileText = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeViewResult
            // 
            this.treeViewResult.Location = new System.Drawing.Point(12, 12);
            this.treeViewResult.Name = "treeViewResult";
            this.treeViewResult.Size = new System.Drawing.Size(348, 397);
            this.treeViewResult.TabIndex = 8;
            // 
            // timerSearch
            // 
            this.timerSearch.Tick += new System.EventHandler(this.timerSearch_Tick);
            // 
            // timerTreeUpdate
            // 
            this.timerTreeUpdate.Tick += new System.EventHandler(this.timerTreeUpdate_Tick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.labelTime,
            this.toolStripStatusLabel,
            this.LabelFileProcessing});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip1.Size = new System.Drawing.Size(800, 22);
            this.statusStrip1.TabIndex = 9;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // labelTime
            // 
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(34, 17);
            this.labelTime.Text = "00:00";
            this.labelTime.Click += new System.EventHandler(this.toolStripStatusLabel1_Click);
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(133, 17);
            this.toolStripStatusLabel.Text = "Обрабатывается файл:";
            // 
            // LabelFileProcessing
            // 
            this.LabelFileProcessing.Name = "LabelFileProcessing";
            this.LabelFileProcessing.Size = new System.Drawing.Size(0, 17);
            // 
            // buttonStartSearch
            // 
            this.buttonStartSearch.Location = new System.Drawing.Point(378, 232);
            this.buttonStartSearch.Name = "buttonStartSearch";
            this.buttonStartSearch.Size = new System.Drawing.Size(341, 23);
            this.buttonStartSearch.TabIndex = 10;
            this.buttonStartSearch.Text = "Старт";
            this.buttonStartSearch.UseSelectable = true;
            this.buttonStartSearch.Click += new System.EventHandler(this.buttonStartSearch_Click);
            // 
            // buttonStopSearch
            // 
            this.buttonStopSearch.Location = new System.Drawing.Point(378, 273);
            this.buttonStopSearch.Name = "buttonStopSearch";
            this.buttonStopSearch.Size = new System.Drawing.Size(341, 23);
            this.buttonStopSearch.TabIndex = 11;
            this.buttonStopSearch.Text = "Стоп";
            this.buttonStopSearch.UseSelectable = true;
            this.buttonStopSearch.Click += new System.EventHandler(this.buttonStopSearch_Click);
            // 
            // textBoxFolderName
            // 
            // 
            // 
            // 
            this.textBoxFolderName.CustomButton.Image = null;
            this.textBoxFolderName.CustomButton.Location = new System.Drawing.Point(322, 1);
            this.textBoxFolderName.CustomButton.Name = "";
            this.textBoxFolderName.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.textBoxFolderName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.textBoxFolderName.CustomButton.TabIndex = 1;
            this.textBoxFolderName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.textBoxFolderName.CustomButton.UseSelectable = true;
            this.textBoxFolderName.CustomButton.Visible = false;
            this.textBoxFolderName.Lines = new string[] {
        "metroTextBox1"};
            this.textBoxFolderName.Location = new System.Drawing.Point(378, 34);
            this.textBoxFolderName.MaxLength = 32767;
            this.textBoxFolderName.Name = "textBoxFolderName";
            this.textBoxFolderName.PasswordChar = '\0';
            this.textBoxFolderName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textBoxFolderName.SelectedText = "";
            this.textBoxFolderName.SelectionLength = 0;
            this.textBoxFolderName.SelectionStart = 0;
            this.textBoxFolderName.ShortcutsEnabled = true;
            this.textBoxFolderName.Size = new System.Drawing.Size(284, 23);
            this.textBoxFolderName.TabIndex = 12;
            this.textBoxFolderName.Text = "metroTextBox1";
            this.textBoxFolderName.UseSelectable = true;
            this.textBoxFolderName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.textBoxFolderName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.textBoxFolderName.TextChanged += new System.EventHandler(this.textBoxFolderName_TextChanged);
            this.textBoxFolderName.Click += new System.EventHandler(this.textBoxFolderName_Click);
            // 
            // textBoxFileTemplate
            // 
            // 
            // 
            // 
            this.textBoxFileTemplate.CustomButton.Image = null;
            this.textBoxFileTemplate.CustomButton.Location = new System.Drawing.Point(319, 1);
            this.textBoxFileTemplate.CustomButton.Name = "";
            this.textBoxFileTemplate.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.textBoxFileTemplate.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.textBoxFileTemplate.CustomButton.TabIndex = 1;
            this.textBoxFileTemplate.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.textBoxFileTemplate.CustomButton.UseSelectable = true;
            this.textBoxFileTemplate.CustomButton.Visible = false;
            this.textBoxFileTemplate.Lines = new string[] {
        "metroTextBox1"};
            this.textBoxFileTemplate.Location = new System.Drawing.Point(378, 102);
            this.textBoxFileTemplate.MaxLength = 32767;
            this.textBoxFileTemplate.Name = "textBoxFileTemplate";
            this.textBoxFileTemplate.PasswordChar = '\0';
            this.textBoxFileTemplate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textBoxFileTemplate.SelectedText = "";
            this.textBoxFileTemplate.SelectionLength = 0;
            this.textBoxFileTemplate.SelectionStart = 0;
            this.textBoxFileTemplate.ShortcutsEnabled = true;
            this.textBoxFileTemplate.Size = new System.Drawing.Size(284, 23);
            this.textBoxFileTemplate.TabIndex = 13;
            this.textBoxFileTemplate.Text = "metroTextBox1";
            this.textBoxFileTemplate.UseSelectable = true;
            this.textBoxFileTemplate.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.textBoxFileTemplate.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.textBoxFileTemplate.TextChanged += new System.EventHandler(this.textBoxFileTemplate_TextChanged);
            // 
            // textBoxFileText
            // 
            // 
            // 
            // 
            this.textBoxFileText.CustomButton.Image = null;
            this.textBoxFileText.CustomButton.Location = new System.Drawing.Point(322, 1);
            this.textBoxFileText.CustomButton.Name = "";
            this.textBoxFileText.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.textBoxFileText.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.textBoxFileText.CustomButton.TabIndex = 1;
            this.textBoxFileText.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.textBoxFileText.CustomButton.UseSelectable = true;
            this.textBoxFileText.CustomButton.Visible = false;
            this.textBoxFileText.Lines = new string[] {
        "metroTextBox1"};
            this.textBoxFileText.Location = new System.Drawing.Point(378, 174);
            this.textBoxFileText.MaxLength = 32767;
            this.textBoxFileText.Name = "textBoxFileText";
            this.textBoxFileText.PasswordChar = '\0';
            this.textBoxFileText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textBoxFileText.SelectedText = "";
            this.textBoxFileText.SelectionLength = 0;
            this.textBoxFileText.SelectionStart = 0;
            this.textBoxFileText.ShortcutsEnabled = true;
            this.textBoxFileText.Size = new System.Drawing.Size(284, 23);
            this.textBoxFileText.TabIndex = 14;
            this.textBoxFileText.Text = "metroTextBox1";
            this.textBoxFileText.UseSelectable = true;
            this.textBoxFileText.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.textBoxFileText.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.textBoxFileText.TextChanged += new System.EventHandler(this.textBoxFileText_TextChanged);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.metroLabel1.Location = new System.Drawing.Point(378, 12);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(151, 19);
            this.metroLabel1.TabIndex = 15;
            this.metroLabel1.Text = "Стартовая директория:";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.metroLabel2.Location = new System.Drawing.Point(378, 80);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(159, 19);
            this.metroLabel2.TabIndex = 16;
            this.metroLabel2.Text = "Шаблон в имени файла:";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.metroLabel3.Location = new System.Drawing.Point(378, 152);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(96, 19);
            this.metroLabel3.TabIndex = 17;
            this.metroLabel3.Text = "Текст в файле:";
            // 
            // Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.textBoxFileText);
            this.Controls.Add(this.textBoxFileTemplate);
            this.Controls.Add(this.textBoxFolderName);
            this.Controls.Add(this.buttonStopSearch);
            this.Controls.Add(this.buttonStartSearch);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.treeViewResult);
            this.Name = "Form";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TreeView treeViewResult;
        private System.Windows.Forms.FolderBrowserDialog folderSelectDialog;
        private System.Windows.Forms.Timer timerSearch;
        private System.Windows.Forms.Timer timerTreeUpdate;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel labelTime;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel LabelFileProcessing;
        private MetroFramework.Controls.MetroButton buttonStartSearch;
        private MetroFramework.Controls.MetroButton buttonStopSearch;
        private MetroFramework.Controls.MetroTextBox textBoxFolderName;
        private MetroFramework.Controls.MetroTextBox textBoxFileTemplate;
        private MetroFramework.Controls.MetroTextBox textBoxFileText;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
    }
}

