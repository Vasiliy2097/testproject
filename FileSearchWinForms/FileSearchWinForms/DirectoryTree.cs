﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security;

namespace FileSearchWinForms
{
    public class DirectoryTree
    {
        string baseDirName;
        string fileNameTemplate;

        public DirectoryTree(string baseDirName, string fileNameTemplate)
        {
            this.baseDirName = baseDirName;
            this.fileNameTemplate = fileNameTemplate;
        }

        public Action<FileInfo> OnFoundFile;

        public Action<Exception> OnException;

        public void Run()
        {
            DirectoryInfo baseDir = new DirectoryInfo(baseDirName);
            Run(baseDir);
        }

        private void Run(DirectoryInfo baseDir)
        {
            try
            {
                foreach (var file in baseDir.EnumerateFiles(fileNameTemplate))
                {
                    if (OnFoundFile != null)
                        OnFoundFile(file);
                }
                foreach (var dir in baseDir.EnumerateDirectories())
                {
                    Run(dir);
                }

            }
            catch (SecurityException e)
            {
                if (OnException != null)
                    OnException(e);
            }
            catch (DirectoryNotFoundException e)
            {
                if (OnException != null)
                    OnException(e);
            }
            catch (System.UnauthorizedAccessException e)
            {
                if (OnException != null)
                    OnException(e);
            }
        }
    }
}
