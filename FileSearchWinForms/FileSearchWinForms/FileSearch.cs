﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileSearchWinForms
{
    public class FileSearch
    {
        public static bool searchInFile(Stream file, IList<byte> template)
        {
            if (file == null || template == null || template.Count == 0)
                return false;

            CircularQueue<byte> buff = new CircularQueue<byte>(template.Count);

            for (int t = file.ReadByte(); t != -1; t = file.ReadByte())
            {
                buff.push((byte)t);

                if (template.Last().Equals((byte)t))
                {
                    if (buff.cmp(template))
                        return true;
                }
            }
            return false;
        }
    }
}
