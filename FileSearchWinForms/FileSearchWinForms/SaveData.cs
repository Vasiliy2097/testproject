﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace FileSearchWinForms
{
    class SaveData
    {
        private string fileName;

        public string _folderName = "";
        public string _fileTemplate = "";
        public string _fileText = "";

        public SaveData(string fileName)
        {
            string folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            this.fileName = folder + "\\" + fileName;
        }

        public void Load()
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                xml.Load(this.fileName);
                _folderName = xml.SelectSingleNode("SaveData/folderName").InnerText;
                _fileTemplate = xml.SelectSingleNode("SaveData/fileTemplate").InnerText;
                _fileText = xml.SelectSingleNode("SaveData/fileText").InnerText;

            }
            catch (Exception e)
            { }
        }

        public void Save()
        {
            try
            {
                XElement xml = new XElement("SaveData"
                    , new XElement("folderName", _folderName)
                    , new XElement("fileTemplate", _fileTemplate)
                    , new XElement("fileText", _fileText));
                xml.Save(fileName);
            }
            catch { }
        }
    }
}
