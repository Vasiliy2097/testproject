﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace FileSearchWinForms
{
    public partial class Form
    {
        private Thread backgroundSearch;

        private DateTime timeStartSearch;

        private string processingFileName;

        private void runSearch(string folderName, string fileTemplate, string fileText)
        {
            var fileTextB = Encoding.UTF8.GetBytes(fileText);

            DirectoryTree tree = new DirectoryTree(folderName, fileTemplate);
            tree.OnFoundFile +=
                file =>
                {
                    try
                    {
                        using (var ff = File.OpenRead(file.FullName))
                        {
                            processingFileName = file.FullName;
                            if (FileSearch.searchInFile(ff, fileTextB))
                            {
                                addFileToTree(file);
                            }

                        }
                    }
                    catch (UnauthorizedAccessException e)
                    { }
                    catch (IOException e)
                    { }
                };
            tree.Run();

            stopTimer();

            // Change LabelFileProcessing to "Done"
            BeginInvoke(new Action<string>
                    ((text) => LabelFileProcessing.Text = text)
                , "Done");

            MessageBox.Show("Поиск завершен");
        }
        Queue<Action> delayedTask = new Queue<Action>();

        private void addFileToTree(FileInfo file)
        {
            lock (delayedTask)
            {
                delayedTask.Enqueue(() =>
                {
                    var dir = file.Directory;
                    TreeNode dirNode = getDirectoryNode(dir);
                    // Add file node if not exist
                    if (treeViewResult.Nodes.Find(file.FullName, true).FirstOrDefault() == null)
                    {
                        dirNode.Nodes.Add(file.FullName, file.Name);
                    }
                });
            }
        }
        private void stopTimer()
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new Action(() =>
                {
                    timerSearch.Stop();
                }));
            }
            else
            {
                timerSearch.Stop();
            }
        }

        
        private TreeNode getDirectoryNode(DirectoryInfo dir)
        {
            var node = treeViewResult.Nodes.Find(dir.FullName.GetHashCode().ToString(), true).FirstOrDefault();
            if (node == null)
            {
                //if (dir.Parent == null) // if it is disk
                if (dir.FullName == textBoxFolderName.Text) // If it is base dir
                {
                    node = treeViewResult.Nodes.Add(dir.FullName.GetHashCode().ToString(), dir.FullName);
                    return node;
                }

                var parentNode = getDirectoryNode(dir.Parent);
                node = parentNode.Nodes.Add(dir.FullName.GetHashCode().ToString(), dir.Name);

            }
            return node;
        }
        private void runDelayedTask()
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new Action(() =>
                {
                    _runDelayedTask();
                }));
            }
            else
            {
                _runDelayedTask();
            }
        }
        private void _runDelayedTask()
        {
            if (delayedTask.Count != 0)
            {
                DateTime now = DateTime.Now;
                treeViewResult.BeginUpdate();
                int i = 0;
                lock (delayedTask)
                {
                    for (i = 0; (DateTime.Now - now).Milliseconds < 700 && delayedTask.Count != 0; i++)
                    {
                        Action tt = delayedTask.Dequeue();
                        tt();
                    }
                }
                treeViewResult.EndUpdate();
                Console.WriteLine("Time2: " + (DateTime.Now - now).Seconds + "   Count:" + delayedTask.Count + "   Done:" + i);
            }
        }
    }
}
