﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FileSearchWinForms.Properties;
using System.Threading;

namespace FileSearchWinForms
{
    public partial class Form : System.Windows.Forms.Form
    {
        private SaveData saveData;
        public Form()
        {
            InitializeComponent();
            timerTreeUpdate.Start();
            saveData = new SaveData(Resources.SaveDataFileName);
            saveData.Load();
            textBoxFolderName.Text = saveData._folderName;
            textBoxFileTemplate.Text = saveData._fileTemplate;
            textBoxFileText.Text = saveData._fileText;
        }

        private void textBoxFolderName_Click(object sender, EventArgs e)
        {
            if (folderSelectDialog.ShowDialog() == DialogResult.OK)
                textBoxFolderName.Text = folderSelectDialog.SelectedPath;
        }

        private void textBoxFolderName_TextChanged(object sender, EventArgs e)
        {
            saveData._folderName = textBoxFolderName.Text;
            saveData.Save();
        }

        private void textBoxFileTemplate_TextChanged(object sender, EventArgs e)
        {
            saveData._fileTemplate = textBoxFileTemplate.Text;
            saveData.Save();
        }

        private void textBoxFileText_TextChanged(object sender, EventArgs e)
        {
            saveData._fileText = textBoxFileText.Text;
            saveData.Save();
        }

        private void buttonStartSearch_Click(object sender, EventArgs e)
        {
            string folderName = textBoxFolderName.Text;
            string fileTemplate = textBoxFileTemplate.Text;
            string fileText = textBoxFileText.Text;

            if (folderName == "" || fileTemplate == "" || fileText == "")
            {
                MessageBox.Show("Для начала поиска необходимо заполнить все поля!");
                return;
            }
            if (backgroundSearch != null && backgroundSearch.IsAlive)
            {
                MessageBox.Show("Поиск уже запущен");
                return;
            }

            treeViewResult.Nodes.Clear();

            labelTime.Text = "--/--";
            timeStartSearch = DateTime.Now;
            timerSearch.Start();

            backgroundSearch = new Thread(() => runSearch(folderName, fileTemplate, fileText));
            backgroundSearch.Start();



        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {
            
        }

        private void buttonStopSearch_Click(object sender, EventArgs e)
        {
            if (backgroundSearch != null && backgroundSearch.IsAlive)
            {
                backgroundSearch.Abort();
                stopTimer();
            }
        }

        private void timerSearch_Tick(object sender, EventArgs e)
        {
            if (timeStartSearch == null)
            {
                labelTime.Text = "--:--";
                return;
            }

            var deltaTime = DateTime.Now - timeStartSearch;
            labelTime.Text = deltaTime.ToString(@"mm\:ss");

            LabelFileProcessing.Text = processingFileName;
        }

        private void Form_Load(object sender, EventArgs e)
        {

        }

        private void timerTreeUpdate_Tick(object sender, EventArgs e)
        {
            runDelayedTask();
        }

        
    }
}
